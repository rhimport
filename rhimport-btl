#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use DBI;
use Date::Calc;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use File::Fetch;
use IO::Handle;
use IPC::Open3;
use HTML::Entities;
use LWP::Simple;

use lib '/usr/local/share/rhimport/';
use rhimport;

my $DBHOST = "airplay";
my $DBUSER = "rivendell";
my $DBPW = "lldriven";
my $DB = "rivendell";

my $STAT_FILE = $ENV{'HOME'} . "/rhimport-btl.stat";
my $RSS_URL = "http://www.btlonline.org/rss/btl128.xml";
my $PV_ID = '221';

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

if($#ARGV >= 0 && $ARGV[0] eq 'last') {
  print "!!!This is the last attempt, there won't be a retry on error!!!\n"
}

my $user = `/usr/bin/id -un`;
$user =~ s/\n//;
my $group = "betweenlin";

my $dbh = DBI->connect("DBI:mysql:$DB:$DBHOST","$DBUSER","$DBPW") or die "Database Error: $DBI::errstr";
my @allowed_dbs = rhimport::get_dropboxes($dbh, $user, $group);

if(scalar(@allowed_dbs) != 1) {
  print "found more or less than one Dropbox for this group?!\n";
  $dbh->disconnect();
  exit 1;
}
my $dropbox = $allowed_dbs[0]->{'PATH'};
my $to_cart = $allowed_dbs[0]->{'TO_CART'};


my @today = Date::Calc::Today();
my @import_date = Date::Calc::Add_Delta_Days(@today, 7);
@import_date = Date::Calc::Standard_to_Business(@import_date);
$import_date[2] = 1;
my @broadcast_date = @import_date;
$broadcast_date[2] = 5;

@import_date = Date::Calc::Business_to_Standard(@import_date);
@broadcast_date = Date::Calc::Business_to_Standard(@broadcast_date);

print "today: " . Date::Calc::Date_to_Text(@today) . "\n";
print "day of next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";
print "ending of current broadcast cycle: " . Date::Calc::Date_to_Text(@broadcast_date) . "\n";

my $id = sprintf("%04d-%02d-%02d", @import_date);
my $bd = sprintf("%04d-%02d-%02d", @broadcast_date);
my $bdfile = sprintf("%02d%02d%02d.*-btlv.*\.mp3", $broadcast_date[0]%100, $broadcast_date[1], $broadcast_date[2]);

print "looking for files like '$bdfile' in RSS Feed\n";
print " -> $RSS_URL\n";

my $feed = XML::Feed->parse(URI->new($RSS_URL))
  or die "Error fetching feed: " . XML::Feed->errstr;

my $file = "";
my $out_file = "";
for my $entry ($feed->entries) {
  if($entry->enclosure && ($entry->enclosure->type eq "audio/mpeg" || entry->enclosure->type eq "audio/mp3")) {
    next unless $entry->enclosure->url =~ /$bdfile/;

    my $sum_title = decode_entities($entry->title);
    $sum_title =~ s/ \(128 kbps\)$//;
    my $sum_text = decode_entities($entry->content->body);

    my $ff = File::Fetch->new(uri => $entry->enclosure->url);
    my $current_file = `cat $STAT_FILE`;
    if($current_file eq $ff->output_file) {
      print "Already downloaded file of today\n";
      $dbh->disconnect();
      exit 0;
    }
    $out_file = $ff->output_file;
    if(!rhimport::check_file_extension($out_file)) {
      print "\n\nThe extension of the matching file '". $out_file . "' seems to be wrong - manual import necessary!!!\n";
      $dbh->disconnect();
      exit 1;
    }

    print $bdfile . ": downloading " . $entry->enclosure->url . " (" . $entry->enclosure->length . " Bytes) .. ";
    $file = $ff->fetch( to => '/tmp' ) or die $ff->error;
    print "ok\n";

    print "summary:\n" . $sum_title . "\n\n" . $sum_text . "\n";

    rhimport::pv_add_note($sum_title, $sum_text, $PV_ID, $id, "1");

    last;
  }
}
if($file eq "" || !(-e "$file")) {
  print "No Entry found for " . Date::Calc::Date_to_Text(@broadcast_date) . " or download error - ";
  if($#ARGV >= 0 && $ARGV[0] eq 'last') {
    print "giving up, manual import necessary!!!\n";
  } else {
    print "will retry later\n";
  }
  exit 1;
}

print "will import $file to dropbox $dropbox (cart=$to_cart)\n";

my $error_cb = sub {
  my ($text) = @_;

  print "\n$text";
  return 0;
};

rhimport::check_key_file() or die "Import Key not found, use rhimport-create-id to create one\n";

my $ret;
my $log = rhimport::clear_carts($dbh, $group, $to_cart);
my $import_log;
($ret, $import_log) = rhimport::import_single($file, $dropbox, $user, 0, $error_cb);
$log .= $import_log;

$dbh->disconnect();

unlink $file;

if(!$ret) {
  print "\nImport Error:\n";
  print $log;
  exit 1;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs "$out_file";
close($fhs);

exit 0;
