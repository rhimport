##
##  rhimport
##
##  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhimport.
##
##  rhimport is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhimport is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
##

ifneq ($(MAKECMDGOALS),distclean)
include include.mk
endif

.PHONY: clean distclean

EXECUTABLE := rhimport

all: $(EXECUTABLE)

distclean: clean
	find . -name "*.\~*" -exec rm -rf {} \;
	rm -f include.mk

clean:

INSTALL_TARGETS := install-bin install-share
REMOVE_TARGETS := remove-bin remove-share

install: $(INSTALL_TARGETS)

install-bin:
	$(INSTALL) -d $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE) $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-btl $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-dn $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-er $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-eu $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-fe $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-fl $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-fw $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-mz $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-nw $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-oi $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-o94n $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-po $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-rs $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-ra $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-sm $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-sv $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-tr $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-ut $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-wr $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-zf $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-zffe $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-zfw $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE)-create-id $(DESTDIR)$(BINDIR)
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-btl
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-dn
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-er
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-eu
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fe
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fl
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fw
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-mz
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-nw
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-oi
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-o94n
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-po
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-rs
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-ra
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-sm
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-sv
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-tr
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-ut
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-wr
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zf
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zffe
	@sed s#/usr/local/share/rhimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zfw

install-share:
	$(INSTALL) -d $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)
	$(INSTALL) -m 644 rhimport.pm $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)
	$(INSTALL) -m 644 gui_callbacks.pm $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)
	$(INSTALL) -m 644 rhimport.glade $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)

uninstall: remove

remove: $(REMOVE_TARGETS)

remove-bin:
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-btl
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-dn
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-er
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-eu
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fe
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fl
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-fw
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-mz
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-nw
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-oi
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-o94n
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-po
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-rs
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-ra
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-sm
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-sv
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-tr
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-ut
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-wr
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zf
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zffe
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-zfw
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)-create-id

remove-share:
	rm -rf $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)
