#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use DBI;
use Date::Calc;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use File::Fetch;
use IO::Handle;
use IPC::Open3;
use HTML::Entities;

use lib '/usr/local/share/rhimport/';
use rhimport;

my $DBHOST = "airplay";
my $DBUSER = "rivendell";
my $DBPW = "lldriven";
my $DB = "rivendell";

my $STAT_FILE = $ENV{'HOME'} . "/rhimport-ut.stat";
my $RSS_URL = "http://cba.fro.at/seriesrss/264016?c=Kfs2IoV2Wmd";
$XML::Feed::MULTIPLE_ENCLOSURES=1;
my $PV_ID = '393';

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

if($#ARGV >= 0 && $ARGV[0] eq 'last') {
  print "!!!This is the last attempt, there won't be a retry on error!!!\n"
}

my $user = `/usr/bin/id -un`;
$user =~ s/\n//;
my $group = "uton";

my $dbh = DBI->connect("DBI:mysql:$DB:$DBHOST","$DBUSER","$DBPW") or die "Database Error: $DBI::errstr";
my @allowed_dbs = rhimport::get_dropboxes($dbh, $user, $group);

my $idx = 0;
my $idx_reb = 1;
if(scalar(@allowed_dbs) != 2) {
  print "found more or less than 2 Dropboxes for this group?!\n";
  $dbh->disconnect();
  exit 1;
}
my $dropbox = $allowed_dbs[$idx]->{'PATH'};
my $to_cart = $allowed_dbs[$idx]->{'TO_CART'};
my $dropbox_reb = $allowed_dbs[$idx_reb]->{'PATH'};
my $to_cart_reb = $allowed_dbs[$idx_reb]->{'TO_CART'};

my @import_date = Date::Calc::Standard_to_Business(Date::Calc::Today());
my $dow = $import_date[2];
$import_date[2] = 3;
@import_date = Date::Calc::Business_to_Standard(@import_date);
@import_date = Date::Calc::Add_Delta_Days(@import_date, 7) if ($dow > 3);

my @broadcast_day = Date::Calc::Add_Delta_Days(@import_date, -106);
my @import_date_reb = Date::Calc::Add_Delta_Days(@import_date, 2);
print "day of original broadcast: " . Date::Calc::Date_to_Text(@broadcast_day) . "\n";
print "day of Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";
print "day of Radio Helsinki rebroadcast: " . Date::Calc::Date_to_Text(@import_date_reb) . "\n";

my $bd = sprintf("%04d-%02d-%02d", $broadcast_day[0], $broadcast_day[1], $broadcast_day[2]);
print "looking for files from $bd in RSS Feed\n";
print " -> $RSS_URL\n";

my $feed = XML::Feed->parse(URI->new($RSS_URL))
  or die "Error fetching feed: " . XML::Feed->errstr;

my $bdexp = sprintf("^UTON%02d%02d%02d", $broadcast_day[0] % 100, $broadcast_day[1], $broadcast_day[2]);

my $file = "";
my $out_file = "";
my $i = 0;
for my $entry ($feed->entries) {
  $i++;
  next unless $entry->enclosure;
  my $j = 0;
  for my $enclosure($entry->enclosure) {
    $j++;
    if($enclosure->type eq "audio/mpeg" || $enclosure->type eq "audio/ogg") {
      print "$i/$j: (" . $enclosure->type . ", " . $enclosure->length . ") "  . $enclosure->url . "\n";
      my $url = $enclosure->url;
      if($url =~ /^(.*)_cut(\.[^\.]+)$/) {
        $url = $1 . $2;
      }

      my $ff = File::Fetch->new(uri => $url);
      next unless (uc($ff->output_file) =~ $bdexp); # file not from correct day

      my $sum_title = decode_entities($entry->title);
      my $sum_text = decode_entities($entry->content->body);

      my $current_file = `cat $STAT_FILE`;
      if($current_file eq $ff->output_file) {
        print "Already downloaded file of today\n";
        $dbh->disconnect();
        exit 0;
      }
      $out_file = $ff->output_file;
      if(!rhimport::check_file_extension($out_file)) {
        print "\n\nThe extension of the matching file '". $out_file . "' seems to be wrong - manual import necessary!!!\n";
        $dbh->disconnect();
        exit 1;
      }

      print " --> " . $bdexp . ": downloading " . $url . " (" . $enclosure->length . " Bytes) .. ";
      $file = $ff->fetch( to => '/tmp' ) or die $ff->error;
      print "ok\n";

      print "summary:\n" . $sum_title . "\n\n" . $sum_text . "\n";

      rhimport::pv_add_note($sum_title, $sum_text, $PV_ID, sprintf("%04d-%02d-%02d", $import_date[0], $import_date[1], $import_date[2]), "1");
      print "\n";
      rhimport::pv_add_note($sum_title, $sum_text, $PV_ID, sprintf("%04d-%02d-%02d", $import_date_reb[0], $import_date_reb[1], $import_date_reb[2]), "2");
      print "\n";

      last;
    }
  }
  last if $file ne "";
}

if($file eq "" || !(-e "$file")) {
  print "No Entry found from $bd or download error - ";
  if($#ARGV >= 0 && $ARGV[0] eq 'last') {
    print "giving up, manual import necessary!!!\n";
  } else {
    print "will retry later\n";
  }
  exit 1;
}

print "will import $file to dropbox $dropbox (cart=$to_cart)\n";
print "will import $file to rebroadcast dropbox $dropbox_reb (cart=$to_cart_reb)\n\n";

my $error_cb = sub {
  my ($text) = @_;

  print "\n$text";
  return 0;
};

rhimport::check_key_file() or die "Import Key not found, use rhimport-create-id to create one\n";

my $ret;
my $log = rhimport::clear_carts($dbh, $group, $to_cart);
my $import_log;
($ret, $import_log) = rhimport::import_single($file, $dropbox, $user, 0, $error_cb);
$log .= $import_log;
$import_log = rhimport::clear_carts($dbh, $group, $to_cart_reb);
$log .= $import_log;
($ret, $import_log) = rhimport::import_single($file, $dropbox_reb, $user, 0, $error_cb);
$log .= $import_log;

$dbh->disconnect();

unlink $file;

if(!$ret) {
  print "\nImport Error:\n";
  print $log;
  exit 1;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs $out_file;
close($fhs);

exit 0;
