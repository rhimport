#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use DBI;
use Date::Calc;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use File::Fetch;
use IO::Handle;
use IPC::Open3;
use HTML::Entities;

$XML::Feed::MULTIPLE_ENCLOSURES=1;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

if(@ARGV == 0) {
  print "You have to supply a Series ID\n";
  exit 1;
}

my $RSS_URL = "http://cba.fro.at/seriesrss/$ARGV[0]?c=Kfs2IoV2Wmd&limit=10000";
print "Fetching files form '$RSS_URL'\n";

my $feed = XML::Feed->parse(URI->new($RSS_URL))
  or die "Error fetching feed: " . XML::Feed->errstr;

my $i = 1;
for my $entry ($feed->entries) {
  if($entry->enclosure) {
    my $sum_title = decode_entities($entry->title);
    my $sum_text = decode_entities($entry->content->body);
    my $j = 1;
    for my $enclosure ($entry->enclosure) {
      if($enclosure->type eq "audio/mpeg" || $enclosure->type eq "audio/ogg") {
        print "$i/$j: (" . $enclosure->type . ", " . $enclosure->length . ") "  . $enclosure->url . "\n";
        my $url = $enclosure->url;
        if($url =~ /^(.*)_cut(\.[^\.]+)$/) {
          $url = $1 . $2;
        }

        # if($i <= 100) {
        #   print" --> skipping " . $url . "\n";
        #   last;
        # }

        my $ff = File::Fetch->new(uri => $url);
        print " --> downloading " . $url . " (" . $enclosure->length . " Bytes) .. ";
        my $file = $ff->fetch() or die $ff->error;
        print "ok\n";
        my $notefile = $file . "_notes";

        unlink($notefile);
        open(my $fhs, '>', $notefile);
        binmode($fhs, ":utf8");
        print $fhs $sum_title . "\n\n" . $sum_text . "\n";
        close($fhs);

        last;
      }
      $j++;
    }
    print "\n";
    $i++;
  }
}
