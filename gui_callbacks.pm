#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;

package gui_callbacks;

sub on_exit
{
  my ($widget, $data) = @_;

  Gtk2->main_quit;
}

sub on_b_mode_clicked
{
  my ($widget, $data) = @_;

  ::toggle_mode_gui();
}

sub on_b_apply_clicked
{
  my ($widget, $data) = @_;
 
  ::start_import_gui();
}

sub on_b_showcarts_clicked
{
  my ($widget, $data) = @_;
 
  ::show_used_carts_gui();
}

sub on_co_dropbox_changed
{
  my ($widget, $data) = @_;

  ::clear_status_gui();
  ::dropbox_updated_gui();
}

sub on_filechooser_selection_changed
{
  my ($widget, $data) = @_;

  ::clear_status_gui();
}

1;
