#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use DBI;
use Date::Calc;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use File::Fetch;
use IO::Handle;
use IPC::Open3;
use HTML::Entities;

$XML::Feed::MULTIPLE_ENCLOSURES=1;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");
$| = 1;

my $RSS_URL_BASE = "http://freie-radios.net/portal/podcast.php?rss&serie=";
my $i = 1;
my $empty = 0;
while($empty < 100) {
  my $feed = XML::Feed->parse(URI->new($RSS_URL_BASE . $i))
    or die "Error fetching feed: " . XML::Feed->errstr;

  if($feed->title eq "freie-radios.net ()") {
    $empty++;
    $i++;
    next;
  } else {
    $empty = 0;
  }

  print $i . ":" . $feed->title . "\n";
  $i++;
}
