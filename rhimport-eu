#!/usr/bin/perl -w
#
#
#  rhimport
#
#  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhimport.
#
#  rhimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use DBI;
use Date::Calc;
use DateTime::Format::ISO8601;
use URI::URL;
use IO::Handle;
use IPC::Open3;
use HTML::Entities;

use lib '/usr/local/share/rhimport/';
use rhimport;

my $DBHOST = "airplay";
my $DBUSER = "rivendell";
my $DBPW = "lldriven";
my $DB = "rivendell";

my $STAT_FILE = $ENV{'HOME'} . "/rhimport-eu.stat";
my $RSS_URL =  "http://economicupdate.libsyn.com/rss";
my $PV_ID = '568';
my $LAST_RUN = 0;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

if($#ARGV >= 0 && $ARGV[0] eq 'last') {
  print "!!!This is the last attempt, there won't be a retry on error!!!\n";
  $LAST_RUN = 1;
}

my $user = `/usr/bin/id -un`;
$user =~ s/\n//;
my $group = "ecoupdate";

my $dbh = DBI->connect("DBI:mysql:$DB:$DBHOST","$DBUSER","$DBPW") or die "Database Error: $DBI::errstr";
my @allowed_dbs = rhimport::get_dropboxes($dbh, $user, $group);

my $idx = 0;
if(scalar(@allowed_dbs) != 1) {
  print "found more or less than 1 Dropbox for this group?!\n";
  $dbh->disconnect();
  exit 1;
}
my $dropbox = $allowed_dbs[$idx]->{'PATH'};
my $to_cart = $allowed_dbs[$idx]->{'TO_CART'};


my @today = Date::Calc::Today();
print "today: " . Date::Calc::Date_to_Text(@today) . "\n";

my @import_date = Date::Calc::Standard_to_Business(@today);
$import_date[2] = 1;
@import_date = Date::Calc::Business_to_Standard(@import_date);

my $dow = Date::Calc::Day_of_Week(@today);
if($dow > 1) {
  @import_date = Date::Calc::Add_Delta_Days(@import_date, 7);
}
my @broadcast_date = Date::Calc::Add_Delta_Days(@import_date, -5);
print "day of next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";
print "day of latest original broadcast before next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@broadcast_date) . "\n";

if(Date::Calc::Delta_Days(@broadcast_date, @today) <= 0) {
  print "File won't be available by now!\n";
  $dbh->disconnect();
  exit 0;
}

my $id = sprintf("%04d-%02d-%02d", @import_date);
my $bd = sprintf("%04d-%02d-%02d", @broadcast_date);
print "looking for files issued after $bd in RSS-Feed\n";
print " -> $RSS_URL\n";

my $feed = rhimport::fetch_parse_rss($RSS_URL)
  or die "Error fetching feed: " . XML::Feed->errstr;

my $file = "";
my $out_file = "";

for my $entry ($feed->entries) {
  next unless $entry->enclosure;

  my $sum_title = decode_entities($entry->title);
  my $issued = $entry->issued;
  next unless (defined $issued);
  my $idt = DateTime::Format::ISO8601->parse_datetime($issued);
  print "  Title: $sum_title (Issued: " . $idt . ")\n";

  my @issued_date = ($idt->year, $idt->month, $idt->day);
  next if(Date::Calc::Delta_Days(@broadcast_date, @issued_date) < 0);

  my $j = 0;
  for my $enclosure($entry->enclosure) {
    $j++;
    if($enclosure->type eq "audio/mpeg" || $enclosure->type eq "audio/ogg") {
      my $orig_uri = $enclosure->url;
      my $uri = new URI::URL($orig_uri);
      my @path = $uri->path_components;

      my $current_file = `cat $STAT_FILE`;
      if($current_file eq $path[-1]) {
        print "Already downloaded file of today\n";
        $dbh->disconnect();
        exit 0;
      }
  $out_file = $path[-1];
      if(!rhimport::check_file_extension($out_file)) {
        print "\n\nThe extension of the matching file '". $out_file . "' seems to be wrong - manual import necessary!!!\n";
        $dbh->disconnect();
        exit 1;
      }
      print " $j: (" . $enclosure->type . ", " . $enclosure->length . ") "  . $out_file . "\n";
      print " --> downloading " . $orig_uri . " .. ";
      $file = "/tmp/" . $out_file;
      system("wget", "--quiet", "--no-check-certificate", $orig_uri, "-O", $file);
      if( $! != 0) {
        $dbh->disconnect();
        die "wget returned with error: " . $!;
      }
      print "ok\n";
      last;
    }
  }

  last unless($file eq "");
}

if($file eq "" || !(-e "$file")) {
  print "No Entry found from $bd or download error - ";
  if($LAST_RUN) {
    print "giving up, manual import necessary!!!\n";
  } else {
    print "will retry later\n";
  }
  $dbh->disconnect();
  exit 1;
}

print "converting $file ... ";
my $infile = $file;
$file =~ s/\.mp3$/.wav/;
system("ffmpeg", "-i", $infile, "-acodec", "pcm_s16le", $file);
if( $! != 0) {
  die "ffmpeg returned with error: " . $!;
}
print "ok\n";
unlink $infile;

print "will import $file to dropbox $dropbox (cart=$to_cart)\n";

my $error_cb = sub {
  my ($text) = @_;

  print "\n$text";
  return 0;
};

rhimport::check_key_file() or die "Import Key not found, use rhimport-create-id to create one\n";

my $ret;
my $log = rhimport::clear_carts($dbh, $group, $to_cart);
my $import_log;
($ret, $import_log) = rhimport::import_single($file, $dropbox, $user, 0, $error_cb);
$log .= $import_log;

$dbh->disconnect();

unlink $file;

if(!$ret) {
  print "\nImport Error:\n";
  print $log;
  exit 1;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs $out_file;
close($fhs);

exit 0;
